package mapreduce

import (
	"fmt"
	"log"
	"sync"
	"time"
)

// schedule starts and waits for all tasks in the given phase (Map or Reduce).
func (mr *Master) schedule(phase jobPhase) {
	var ntasks int
	var nios int // number of inputs (for reduce) or outputs (for map)
	switch phase {
	case mapPhase:
		ntasks = len(mr.files)
		nios = mr.nReduce
	case reducePhase:
		ntasks = mr.nReduce
		nios = len(mr.files)
	}

	fmt.Printf("Schedule: %v %v tasks (%d I/Os)\n", ntasks, phase, nios)
	defer fmt.Printf("Schedule: %v phase done\n", phase)

	// All ntasks tasks have to be scheduled on workers, and only once all of
	// them have been completed successfully should the function return.
	// Remember that workers may fail, and that any given worker may finish
	// multiple tasks.
	//
	// TODO TODO TODO TODO TODO TODO TODO TODO TODO TODO TODO TODO TODO
	//

	chTasks := make(chan DoTaskArgs, ntasks)
	chAllDone := make(chan bool)
	remain, rmThread := ntasks, 0
	var remainMutex, rmThreadMutex sync.Mutex

	switch phase {
	case mapPhase:
		for i := 0; i < ntasks; i++ {
			chTasks <- DoTaskArgs{mr.jobName, mr.files[i], phase, i, nios}
		}
		mr.remainedWorkers = []string{}
	case reducePhase:
		for i := 0; i < ntasks; i++ {
			chTasks <- DoTaskArgs{mr.jobName, "", phase, i, nios}
		}
	}

	if remain == 0 {
		return
	}

	funcWorker := func(wk string) {
		log.Printf("[Worker %s]Started\n", wk)
		var reply struct{}
		for {
			args, ok := <-chTasks
			if !ok {
				break
			}
			log.Printf("[Worker %s]Do Task %v\n", wk, args)
			if !call(wk, "Worker.DoTask", &args, &reply) {
				log.Printf("[Worker %s]Call Failed\n", wk)
				chTasks <- args
				time.Sleep(time.Millisecond * 250)
				continue
			}
			remainMutex.Lock()
			remain--
			if remain == 0 {
				close(chTasks)
			}
			log.Printf("Remain %d Tasks\n", remain)
			remainMutex.Unlock()
		}

		mr.Lock()
		mr.remainedWorkers = append(mr.remainedWorkers, wk)
		mr.Unlock()

		rmThreadMutex.Lock()
		rmThread--
		if rmThread == 0 {
			chAllDone <- true
		}
		rmThreadMutex.Unlock()
		log.Printf("[Worker %s]Stopped\n", wk)
	}

	mr.Lock()
	for _, v := range mr.remainedWorkers {
		rmThreadMutex.Lock()
		rmThread++
		go funcWorker(v)
		rmThreadMutex.Unlock()
	}
	mr.Unlock()
	for {
		select {
		case <-chAllDone:
			log.Printf("All Done.\n")
			return
		case workerName := <-mr.registerChannel:
			rmThreadMutex.Lock()
			rmThread++
			select {
			case <-chAllDone:
				rmThreadMutex.Unlock()
				mr.Lock()
				mr.remainedWorkers = append(mr.remainedWorkers, workerName)
				mr.Unlock()
				return
			default:
				go funcWorker(workerName)
				rmThreadMutex.Unlock()
			}
		}
	}
}
