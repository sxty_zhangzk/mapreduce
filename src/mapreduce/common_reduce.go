package mapreduce

import (
	"encoding/json"
	"log"
	"os"
	"sort"
)

type kvList []KeyValue

func (list *kvList) Less(i, j int) bool {
	return (*list)[i].Key < (*list)[j].Key
}
func (list *kvList) Len() int {
	return len(*list)
}
func (list *kvList) Swap(i, j int) {
	(*list)[i], (*list)[j] = (*list)[j], (*list)[i]
}

// doReduce does the job of a reduce worker: it reads the intermediate
// key/value pairs (produced by the map phase) for this task, sorts the
// intermediate key/value pairs by key, calls the user-defined reduce function
// (reduceF) for each key, and writes the output to disk.
func doReduce(
	jobName string, // the name of the whole MapReduce job
	reduceTaskNumber int, // which reduce task this is
	nMap int, // the number of map tasks that were run ("M" in the paper)
	reduceF func(key string, values []string) string,
) {
	// TODO:
	// You will need to write this function.
	// You can find the intermediate file for this reduce task from map task number
	// m using reduceName(jobName, m, reduceTaskNumber).
	// Remember that you've encoded the values in the intermediate files, so you
	// will need to decode them. If you chose to use JSON, you can read out
	// multiple decoded values by creating a decoder, and then repeatedly calling
	// .Decode() on it until Decode() returns an error.
	//
	// You should write the reduced output in as JSON encoded KeyValue
	// objects to a file named mergeName(jobName, reduceTaskNumber). We require
	// you to use JSON here because that is what the merger than combines the
	// output from all the reduce tasks expects. There is nothing "special" about
	// JSON -- it is just the marshalling format we chose to use. It will look
	// something like this:
	//
	// enc := json.NewEncoder(mergeFile)
	// for key in ... {
	// 	enc.Encode(KeyValue{key, reduceF(...)})
	// }
	// file.Close()

	var kv, mergekv kvList
	for i := 0; i < nMap; i++ {
		fname := reduceName(jobName, i, reduceTaskNumber)
		fin, err := os.Open(fname)
		if err != nil {
			log.Fatal("[doReduce]Cannot Open File: ", fname, " (", err, ")")
		}

		stat, err := fin.Stat()
		if err != nil {
			log.Fatal("[doReduce]Cannot Get File Stat: ", err)
		}
		if stat.Size() == 0 {
			fin.Close()
			continue
		}

		var tmp KeyValue
		dec := json.NewDecoder(fin)
		/*if err = dec.Decode(&tmp); err != nil {
			fin.Close()
			log.Fatal("[doReduce]Cannot Parse JSON: ", err)
		}*/
		for dec.Decode(&tmp) == nil {
			kv = append(kv, tmp)
		}
		fin.Close()
	}

	sort.Sort(&kv)

	for i, last := 0, 0; i < len(kv); i++ {
		if i+1 == len(kv) || kv[i+1].Key != kv[i].Key {
			var tmp []string
			for j := last; j <= i; j++ {
				tmp = append(tmp, kv[j].Value)
			}
			mergekv = append(mergekv, KeyValue{kv[i].Key, reduceF(kv[i].Key, tmp)})
			last = i + 1
		}
	}

	fout, err := os.Create(mergeName(jobName, reduceTaskNumber))
	if err != nil {
		log.Fatal("[doReduce]Cannot Create File: ", mergeName(jobName, reduceTaskNumber), " (", err, ")")
	}
	defer fout.Close()

	enc := json.NewEncoder(fout)
	for _, kv := range mergekv {
		enc.Encode(&kv)
	}
}
